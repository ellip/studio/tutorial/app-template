import os
import sys
import logging
from click2cwl import dump

logging.basicConfig(
    stream=sys.stderr,
    level=logging.DEBUG,
    format="%(asctime)s %(levelname)-8s %(message)s",
    datefmt="%Y-%m-%dT%H:%M:%S",
)


@click.command(
    short_help="{{cookiecutter.project_title}}",
    help="{{cookiecutter.project_description}}",
    context_settings=dict(
        ignore_unknown_options=True,
        allow_extra_args=True,
    ),
)
@click.option(
    "--input_reference",
    "-i",
    "input_reference",
    help="Input product reference",
    type=click.Path(),
    required=True,
)
@click.option(
    "--aoi",
    "-a",
    "aoi",
    help="Area of interest in Well-known Text (WKT)",
    required=False,
    default=None,
)
@click.pass_context
def main(ctx, input_reference, aoi):

    dump(ctx)

    logging.info("Hello World!")


if __name__ == "__main__":
    main()
